let animation = {
    "v": "5.5.6",
    "fr": 25,
    "ip": 0,
    "op": 150,
    "w": 500,
    "h": 500,
    "nm": "Composition 1",
    "ddd": 0,
    "assets": [],
    "layers": [{
        "ddd": 0,
        "ind": 1,
        "ty": 4,
        "nm": "secondary",
        "sr": 1,
        "ks": {
            "o": {"a": 0, "k": 100, "ix": 11},
            "r": {"a": 0, "k": 0, "ix": 10},
            "p": {
                "a": 1,
                "k": [{
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 0,
                    "s": [275, 296, 0],
                    "to": [0, 1.667, 0],
                    "ti": [0, 0, 0]
                }, {
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 66,
                    "s": [275, 306, 0],
                    "to": [0, 0, 0],
                    "ti": [0, 1.667, 0]
                }, {"t": 149, "s": [275, 296, 0]}],
                "ix": 2
            },
            "a": {"a": 0, "k": [25, 46, 0], "ix": 1},
            "s": {
                "a": 1,
                "k": [{
                    "i": {"x": [0.833, 0.833, 0.833], "y": [0.833, 0.833, 0.833]},
                    "o": {"x": [0.167, 0.167, 0.167], "y": [0.167, 0.167, 0.167]},
                    "t": 0,
                    "s": [100, 100, 100]
                }, {
                    "i": {"x": [0.833, 0.833, 0.833], "y": [0.833, 0.833, 0.833]},
                    "o": {"x": [0.167, 0.167, 0.167], "y": [0.167, 0.167, 0.167]},
                    "t": 66,
                    "s": [100, -123.301, 100]
                }, {"t": 149, "s": [100, 100, 100]}],
                "ix": 6
            }
        },
        "ao": 0,
        "shapes": [{
            "ty": "gr",
            "it": [{
                "ind": 0,
                "ty": "sh",
                "ix": 1,
                "ks": {
                    "a": 0,
                    "k": {
                        "i": [[0, 0], [0, 0], [14, -10], [0, 0], [-25, 20]],
                        "o": [[0, 0], [0, 0], [-14, 10], [0, 0], [25, -20]],
                        "v": [[51, 46], [-5, 46], [-10, 114], [-82, 149], [24, 119]],
                        "c": true
                    },
                    "ix": 2
                },
                "nm": "Tracé 1",
                "mn": "ADBE Vector Shape - Group",
                "hd": false
            }, {
                "ty": "fl",
                "c": {"a": 0, "k": [0.890196078431, 0.39797001259, 0, 1], "ix": 4},
                "o": {"a": 0, "k": 100, "ix": 5},
                "r": 1,
                "bm": 0,
                "nm": "secondaryColor",
                "mn": "ADBE Vector Graphic - Fill",
                "hd": false
            }, {
                "ty": "tr",
                "p": {"a": 0, "k": [0, 0], "ix": 2},
                "a": {"a": 0, "k": [0, 0], "ix": 1},
                "s": {"a": 0, "k": [100, 100], "ix": 3},
                "r": {"a": 0, "k": 0, "ix": 6},
                "o": {"a": 0, "k": 100, "ix": 7},
                "sk": {"a": 0, "k": 0, "ix": 4},
                "sa": {"a": 0, "k": 0, "ix": 5},
                "nm": "Transform"
            }],
            "nm": "Forme 1",
            "np": 3,
            "cix": 2,
            "bm": 0,
            "ix": 1,
            "mn": "ADBE Vector Group",
            "hd": false
        }],
        "ip": 0,
        "op": 150,
        "st": 0,
        "bm": 0
    }, {
        "ddd": 0,
        "ind": 2,
        "ty": 4,
        "nm": "Calque de forme 4",
        "sr": 1,
        "ks": {
            "o": {"a": 0, "k": 100, "ix": 11},
            "r": {"a": 0, "k": 0, "ix": 10},
            "p": {
                "a": 1,
                "k": [{
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 0,
                    "s": [250, 250, 0],
                    "to": [0, 0.833, 0],
                    "ti": [0, 0, 0]
                }, {
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 66,
                    "s": [250, 255, 0],
                    "to": [0, 0, 0],
                    "ti": [0, 0.833, 0]
                }, {"t": 149, "s": [250, 250, 0]}],
                "ix": 2
            },
            "a": {"a": 0, "k": [0, 0, 0], "ix": 1},
            "s": {"a": 0, "k": [100, 100, 100], "ix": 6}
        },
        "ao": 0,
        "shapes": [{
            "ty": "gr",
            "it": [{
                "d": 1,
                "ty": "el",
                "s": {"a": 0, "k": [18, 18], "ix": 2},
                "p": {"a": 0, "k": [0, 0], "ix": 3},
                "nm": "Tracé d'ellipse 1",
                "mn": "ADBE Vector Shape - Ellipse",
                "hd": false
            }, {
                "ty": "fl",
                "c": {"a": 0, "k": [0.827450980392, 0.827450980392, 0.827450980392, 1], "ix": 4},
                "o": {"a": 0, "k": 100, "ix": 5},
                "r": 1,
                "bm": 0,
                "nm": "Remplissage 1",
                "mn": "ADBE Vector Graphic - Fill",
                "hd": false
            }, {
                "ty": "tr",
                "p": {"a": 0, "k": [79, 25], "ix": 2},
                "a": {"a": 0, "k": [0, 0], "ix": 1},
                "s": {"a": 0, "k": [100, 100], "ix": 3},
                "r": {"a": 0, "k": 0, "ix": 6},
                "o": {"a": 0, "k": 100, "ix": 7},
                "sk": {"a": 0, "k": 0, "ix": 4},
                "sa": {"a": 0, "k": 0, "ix": 5},
                "nm": "Transform"
            }],
            "nm": "Ellipse 1",
            "np": 3,
            "cix": 2,
            "bm": 0,
            "ix": 1,
            "mn": "ADBE Vector Group",
            "hd": false
        }],
        "ip": 0,
        "op": 150,
        "st": 0,
        "bm": 0
    }, {
        "ddd": 0,
        "ind": 3,
        "ty": 4,
        "nm": "primary",
        "sr": 1,
        "ks": {
            "o": {"a": 0, "k": 100, "ix": 11},
            "r": {"a": 0, "k": 0, "ix": 10},
            "p": {
                "a": 1,
                "k": [{
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 0,
                    "s": [250, 250, 0],
                    "to": [0, 1.667, 0],
                    "ti": [0, 0, 0]
                }, {
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 66,
                    "s": [250, 260, 0],
                    "to": [0, 0, 0],
                    "ti": [0, 1.667, 0]
                }, {"t": 149, "s": [250, 250, 0]}],
                "ix": 2
            },
            "a": {"a": 0, "k": [0, 0, 0], "ix": 1},
            "s": {"a": 0, "k": [100, 100, 100], "ix": 6}
        },
        "ao": 0,
        "shapes": [{
            "ty": "gr",
            "it": [{
                "ind": 0,
                "ty": "sh",
                "ix": 1,
                "ks": {
                    "a": 0,
                    "k": {
                        "i": [[-3, -5], [-13, -10], [-33, -12], [-22, 3], [0, 0], [14, 2], [57, -10], [42, 9]],
                        "o": [[3, 5], [13, 10], [33, 12], [22, -3], [0, 0], [-14, -2], [-57, 10], [-42, -9]],
                        "v": [[-159, -6], [-96, 54], [-5, 76], [88, 68], [128, 35], [75, -6], [-32, 5], [-111, 12]],
                        "c": true
                    },
                    "ix": 2
                },
                "nm": "Tracé 1",
                "mn": "ADBE Vector Shape - Group",
                "hd": false
            }, {
                "ty": "fl",
                "c": {"a": 0, "k": [0.196077997544, 0.784313964844, 0.196077997544, 1], "ix": 4},
                "o": {"a": 0, "k": 100, "ix": 5},
                "r": 1,
                "bm": 0,
                "nm": "primaryColor",
                "mn": "ADBE Vector Graphic - Fill",
                "hd": false
            }, {
                "ty": "tr",
                "p": {"a": 0, "k": [0, 0], "ix": 2},
                "a": {"a": 0, "k": [0, 0], "ix": 1},
                "s": {"a": 0, "k": [100, 100], "ix": 3},
                "r": {"a": 0, "k": 0, "ix": 6},
                "o": {"a": 0, "k": 100, "ix": 7},
                "sk": {"a": 0, "k": 0, "ix": 4},
                "sa": {"a": 0, "k": 0, "ix": 5},
                "nm": "Transform"
            }],
            "nm": "Forme 1",
            "np": 3,
            "cix": 2,
            "bm": 0,
            "ix": 1,
            "mn": "ADBE Vector Group",
            "hd": false
        }],
        "ip": 0,
        "op": 150,
        "st": 0,
        "bm": 0
    }, {
        "ddd": 0,
        "ind": 4,
        "ty": 4,
        "nm": "secondary",
        "sr": 1,
        "ks": {
            "o": {"a": 0, "k": 100, "ix": 11},
            "r": {"a": 0, "k": 0, "ix": 10},
            "p": {
                "a": 1,
                "k": [{
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 0,
                    "s": [258, 274, 0],
                    "to": [0, 1.667, 0],
                    "ti": [0, 0, 0]
                }, {
                    "i": {"x": 0.833, "y": 0.833},
                    "o": {"x": 0.167, "y": 0.167},
                    "t": 66,
                    "s": [258, 284, 0],
                    "to": [0, 0, 0],
                    "ti": [0, 1.667, 0]
                }, {"t": 149, "s": [258, 274, 0]}],
                "ix": 2
            },
            "a": {"a": 0, "k": [22, 43, 0], "ix": 1},
            "s": {
                "a": 1,
                "k": [{
                    "i": {"x": [0.833, 0.833, 0.833], "y": [0.833, 0.833, 0.833]},
                    "o": {"x": [0.167, 0.167, 0.167], "y": [0.167, 0.167, 0.167]},
                    "t": 0,
                    "s": [100, 100, 100]
                }, {
                    "i": {"x": [0.833, 0.833, 0.833], "y": [0.833, 0.833, 0.833]},
                    "o": {"x": [0.167, 0.167, 0.167], "y": [0.167, 0.167, 0.167]},
                    "t": 66,
                    "s": [100, -75.472, 100]
                }, {"t": 149, "s": [100, 100, 100]}],
                "ix": 6
            }
        },
        "ao": 0,
        "shapes": [{
            "ty": "gr",
            "it": [{
                "ind": 0,
                "ty": "sh",
                "ix": 1,
                "ks": {
                    "a": 0,
                    "k": {
                        "i": [[0, 0], [0, 0], [14, -10], [0, 0], [-25, 20]],
                        "o": [[0, 0], [0, 0], [-14, 10], [0, 0], [25, -20]],
                        "v": [[51, 46], [-5, 46], [-10, 114], [-82, 149], [24, 119]],
                        "c": true
                    },
                    "ix": 2
                },
                "nm": "aaaaa",
                "mn": "ADBE Vector Shape - Group",
                "hd": false
            }, {
                "ty": "fl",
                "c": {"a": 0, "k": [0.890196078431, 0.39797001259, 0, 1], "ix": 4},
                "o": {"a": 0, "k": 100, "ix": 5},
                "r": 1,
                "bm": 0,
                "nm": "secondaryColor",
                "mn": "ADBE Vector Graphic - Fill",
                "hd": false
            }, {
                "ty": "tr",
                "p": {"a": 0, "k": [0, 0], "ix": 2},
                "a": {"a": 0, "k": [0, 0], "ix": 1},
                "s": {"a": 0, "k": [100, 100], "ix": 3},
                "r": {"a": 0, "k": 0, "ix": 6},
                "o": {"a": 0, "k": 100, "ix": 7},
                "sk": {"a": 0, "k": 0, "ix": 4},
                "sa": {"a": 0, "k": 0, "ix": 5},
                "nm": "Transform"
            }],
            "nm": "Forme 1",
            "np": 3,
            "cix": 2,
            "bm": 0,
            "ix": 1,
            "mn": "ADBE Vector Group",
            "hd": false
        }],
        "ip": 0,
        "op": 150,
        "st": 0,
        "bm": 0
    }],
    "markers": []
};