let rgbaPrimaryColor = [40, 180, 153, 1];
let rgbaSecondaryColor = [20, 255, 153, 1];
let speed = 10;

let primaryColorArray = [];
let secondaryColorArray = [];

function rgbaToVectorColor(rgbaColorArray) {
    for (let i = 0; i < rgbaColorArray.length; i++) {
        rgbaColorArray[i] = (rgbaColorArray[i] / 255);
    }
}

function lookForColorLayer(obj, colorTypeValue, destination) {
    for (let key in obj) {
        let value = obj[key];
        if (value === colorTypeValue) {
            destination.push(obj);
        }
        if (typeof value === "object") {
            lookForColorLayer(value, colorTypeValue, destination);
        }
    }
}

lookForColorLayer(animation, 'primaryColor', primaryColorArray);
lookForColorLayer(animation, 'secondaryColor', secondaryColorArray);

if (primaryColorArray.length > 0) {
    rgbaToVectorColor(rgbaPrimaryColor);
    primaryColorArray.forEach(obj => {
        obj.c.k = rgbaPrimaryColor;
    })
}

if (secondaryColorArray.length > 0) {
    rgbaToVectorColor(rgbaSecondaryColor);
    secondaryColorArray.forEach(obj => {
        obj.c.k = rgbaSecondaryColor;
    })
}

let anim;
let animData = {
    container: document.getElementById('bm'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: animation,
    rendererSettings: {
        className: 'animation'
    }
};

anim = bodymovin.loadAnimation(animData);
anim.setSpeed(speed);
